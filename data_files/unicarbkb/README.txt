### README


### Attribution
If using this data you are required to attribute the source including database name (UniCarbKB) and provide a link to the relevant data collections. 
https://academic.oup.com/nar/article/42/D1/D215/1052197
https://doi.org/10.1093/nar/gkt1128


### Object Storage
All triplestore build files (ttl's) are stored in a NeCTAR Object Storage bucket (https://dashboard.rc.nectar.org.au/project/containers/container/glygen_ttl/unicarbkbv2.0.1)
These are being updated on a regulary basis - use at own risk - all work need to be cited and attributed.