Enzyme : FUT1
Substrate : \(Ab3GNb
Product : (Fa2Ab3GNb
Constraint : none
EC : 2.4.1.69
IR : 19
/
Enzyme : FUT2
Substrate : \(Ab4GNb
Product : (Fa2Ab4GNb
Constraint : none
EC : 2.4.1.69
IR : 13
/
Enzyme : ALG10
Substrate : \(Ga3Ga3Ma2Ma2Ma3:q!
:q!
:q!

Product : (Ga2Ga3Ga3Ma2Ma2Ma3
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /\Q$match\E(\(GNb4\))?\(($pat)?(\($pat\))?Ma6\)Mb4GNb4GN/
EC : 2.4.1.256
IR : 44
/
Enzyme : ALG11I
Substrate : \(Ma3\(
Product : (Ma2Ma3(
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /\Q$match\E(GNb4\))?($pat)?(\($pat\))?Ma6\)Mb4GNb4GN/
EC : 2.4.1.131
IR : 38
/
Enzyme : ALG11II
Substrate : \(Ma2Ma3
Product : (Ma2Ma2Ma3
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /^\Q$match\E(\(GNb4\))?\(($pat)(\($pat\))?Ma6\)Mb4GNb4GN/
EC : 2.4.1.131
IR : 39
/
Enzyme : ALG12
Substrate : \(_Ma3Ma6\)Mb4GNb4GN;
Product : ( Ma3(Ma6)Ma6)Mb4GNb4GN;
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /Ma3(\(GNb4\))?\Q$match\E$/ & ($space eq '' | $space =~ /^[^ANGF]+$/)
EC : 2.4.1.260
IR : 25
/
Enzyme : ALG3
Substrate : \(Ma6\)Mb4GNb4GN
Product : (Ma3Ma6)Mb4GNb4GN
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /Ma3(\(GNb4\))?\Q$match\E/
EC : 2.4.1.258
IR : 24
/
Enzyme : ALG6
Substrate : \(Ma2Ma2Ma3
Product : (Ga3Ma2Ma2Ma3
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /\Q$match\E(\(GNb4\))?\(($pat)?(\($pat\))?Ma6\)Mb4GNb4GN/
EC : 2.4.1.267
IR : 42
/
Enzyme : ALG8
Substrate : \(Ga3Ma2Ma2Ma3
Product : (Ga3Ga3Ma2Ma2Ma3
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /\Q$match\E(\(GNb4\))?\(($pat)?(\($pat\))?Ma6\)Mb4GNb4GN/
EC : 2.4.1.265
IR : 43
/
Enzyme : ST3GAL2
Substrate : \(Ab3
Product : (NNa3Ab3
Constraint : none
EC : 2.4.99.4
IR : 15
/
Enzyme : ALG9I
Substrate : \(Ma3Ma6\)Mb4GNb4GN
Product : (Ma2Ma3Ma6)Mb4GNb4GN
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /Ma3(\(GNb4\))?\Q$match\E/
EC : 2.4.1.259
IR : 40
/
Enzyme : ALG9II
Substrate : \(_Ma3\(Ma6\)Ma6\)Mb4GNb4GN
Product : ( Ma3(Ma2Ma6)Ma6)Mb4GNb4GN
Constraint : $oya !~ /GN.+Mb4GNb4GN/ & $oya !~ /AN/ & $oya !~ /A/ & $oya !~ /F/ & $oya !~ /NN/ & $oya =~ /Ma3(\(GNb4\))?\Q$match\E/ & $space eq '' | $space =~ /^[^ANGF]+$/
EC : 2.4.1.261
IR : 41
/
Enzyme : B3GNT1
Substrate : \(Ab4GN
Product : (GNb3Ab4GN
Constraint : none
EC : 2.4.1.149
IR : 28
/
Enzyme : B3GNT7
Substrate : \(AN
Product : (GNb3AN
Constraint : none
EC : none
IR : 9
/
Enzyme : B3GALT
Substrate : \(GNb
Product : (Ab3GNb
Constraint : $oya !~ /(\Q$match\E)4\)\(($pat)Ma6\)Mb4GNb4/
EC : none
IR : 26
/
Enzyme : B4GALNT
Substrate : \(GN
Product : (ANb4GN
Constraint : $oya !~ /(\Q$match\E)b4\)\(($pat)Ma6\)Mb4GNb4/
EC : 2.4.1.244
IR : 5
/
Enzyme : B4GALNT1
Substrate : \(Ab4
Product : (ANb4Ab4
Constraint : none
EC : 2.4.1.92
IR : 6
/
Enzyme : B4GALTI
Substrate : \(GN
Product : (Ab4GN
Constraint : $oya !~ /^\Q$left$match\Eb4\)\(($pat)Ma6\)Mb4GNb4/ & $left !~ /Ma3$/
EC : 2.4.1.38,2.4.1.90
IR : 27,3
/
Enzyme : B4GALTII
Substrate : \(Ga
Product : (Ab4Ga
Constraint : none
EC : 2.4.1.22
IR : 2
/
Enzyme : B4GALTIII
Substrate : \(GN
Product : (Ab4GN
Constraint : $oya !~ /Ma3\Q$match\Eb4\)\(($pat)Ma6\)Mb4GNb4/
EC : 2.4.1.38,2.4.1.90
IR : 27,3
/
Enzyme : ABOI
Substrate : \(Fa2Ab
Product : (ANa3(Fa2)Ab
Constraint : none
EC : 2.4.1.40
IR : 4
/
Enzyme : ABOII
Substrate : \(Fa2Ab
Product : (Aa3(Fa2)Ab
Constraint : none
EC : 2.4.1.37
IR : 1
/
Enzyme : FUTIII
Substrate : Ab3GNb
Product : Ab3(Fa4)GNb
Constraint : $left =~ /(\(|(\(Fa2)|(\(NNa3))$/
EC : 2.4.1.65
IR : 34
/
Enzyme : FUTIV
Substrate : \(-Ab4GNb
Product : ( Ab4(Fa3)GNb
Constraint : $space eq '' | $space eq 'Fa2' | $space eq 'NNa3'
EC : 2.4.1.65
IR : 35
/
Enzyme : FUT11I
Substrate : \(-Ab4GNb
Product : ( Ab4(Fa3)GNb
Constraint : $space eq '' | $space eq 'Fa2'
EC : 2.4.1.152
IR : 33
/
Enzyme : FUT8
Substrate : GNb4GN
Product : GNb4(Fa6)GN
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/ & $oya !~ /Ab/
EC : 2.4.1.68
IR : 32
/
Enzyme : GCNT3
Substrate : \(Ab4GN
Product : (GNb6Ab4GN
Constraint : none
EC : 2.4.1.102
IR : 12
/
Enzyme : GCNT2
Substrate : \(Ab4GNb3Ab
Product : (Ab4GNb3(GNb6)Ab
Constraint : none
EC : 2.4.1.150
IR : 11
/
Enzyme : MGAT1
Substrate : \(Ma3
Product : (GNb2Ma3
Constraint : $oya =~ /\Q$match\E(\(GNb4\))?\(Ma6\)Mb4GNb4/
EC : 2.4.1.101
IR : 7
/
Enzyme : MGAT2
Substrate : \(Ma6\)Mb4GNb4
Product : (GNb2Ma6)Mb4GNb4
Constraint : $oya =~ /(GNb2)?Ma3(\(GNb4\))?\Q$match\E/
EC : 2.4.1.143
IR : 8
/
Enzyme : MGAT3
Substrate : \(_Ma6\)Mb4GNb4
Product : (GNb4)( Ma6)Mb4GNb4
Constraint : $oya !~ /Ab/ & $oya !~ /Ma3\(GNb4\)\($pat/
EC : 2.4.1.144
IR : 31
/
Enzyme : MGAT4I
Substrate : \(GNb2Ma3
Product : (GNb2(GNb4)Ma3
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/
EC : 2.4.1.145
IR : 29
/
Enzyme : MGAT4II
Substrate : \(_Ma3\(GNb2\(GNb6\)Ma6\)Mb4GNb4GN
Product : ( Ma3(GNb2(GNb4)(GNb6)Ma6)Mb4GNb4GN
Constraint : $space eq '' | $space =~ /GNb2(\(GNb4\))?$/
EC : 2.4.1.201
IR : 10
/
Enzyme : MGAT4III
Substrate : \(GNb2\(GNb6\)Ma3
Product : (GNb2(GNb4)(GNb6)Ma3
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/
EC : none
IR : 29
/
Enzyme : MGAT4IV
Substrate : \(_Ma3\(GNb2Ma6\)Mb4GNb4GN
Product : ( Ma3(GNb2(GNb4)Ma6)Mb4GNb4GN
Constraint : $space eq '' | $space =~ /GNb2(\(GNb4\))?$/
EC : none
IR : 10
/
Enzyme : MGAT5I
Substrate : \(GNb2Ma6
Product : (GNb2(GNb6)Ma6
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/
EC : 2.4.1.155
IR : 30
/
Enzyme : MGAT5II
Substrate : \(GNb2Ma3
Product : (GNb2(GNb6)Ma3
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/
EC : none
IR : 30
/
Enzyme : MGAT5III
Substrate : \(GNb2\(GNb4\)Ma6
Product : (GNb2(GNb4)(GNb6)Ma6
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/
EC : none
IR : 30
/
Enzyme : MGAT5IV
Substrate : \(GNb2\(GNb4\)Ma3
Product : (GNb2(GNb4)(GNb6)Ma3
Constraint : $oya !~ /Ma3\(GNb4\)\(($pat)Ma6\)Mb4/
EC : none
IR : 30
/
Enzyme : ST6GAL
Substrate : \(Ab4GN
Product : (NNa6Ab4GN
Constraint : none
EC : 2.4.99.1
IR : 17
/
Enzyme : ST3GAL3
Substrate : \(Ab4GN
Product : (NNa3Ab4GN
Constraint : none
EC : 2.4.99.6
IR : 14
/
Enzyme : ST3GAL
Substrate : \(Ab4GNb3
Product : (NNa3Ab4GNb3
Constraint : none
EC : 2.4.99.10
IR : 16
/
Enzyme : ST8SIAI
Substrate : \(NNa3Ab
Product : (NNa8NNa3Ab
Constraint : none
EC : 2.4.99.-
IR : 18
/
Enzyme : ST8SIAII
Substrate : \(NNa8NNa3
Product : (NNa8NNa8NNa3
Constraint : none
EC : 2.4.99.-
IR : 36
/
Enzyme : ST8SIAIV
Substrate : \(NNa8NNa6
Product : (NNa8NNa8NNa6
Constraint : none
EC : 2.4.99.-
IR : 36
/
Enzyme : ST8SIAIII
Substrate : \(NNa6Ab
Product : (NNa8NNa6Ab
Constraint : none
EC : 2.4.99.-
IR : 20
/
///
