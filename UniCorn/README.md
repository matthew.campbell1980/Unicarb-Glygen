### Comprehensive analysis of the N-glycan biosynthetic pathway using bioinformatics to generate UniCorn: A theoretical N-glycan structure database

#### Published Abstract
Glycan structures attached to proteins are comprised of diverse monosaccharide sequences and linkages that are produced from precursor nucleotide-sugars by a series of glycosyltransferases. Databases of these structures are an essential resource for the interpretation of analytical data and the development of bioinformatics tools. However, with no template to predict what structures are possible the human glycan structure databases are incomplete and rely heavily on the curation of published, experimentally determined, glycan structure data. In this work, a library of 45 human glycosyltransferases was used to generate a theoretical database of N-glycan structures comprised of 15 or less monosaccharide residues. Enzyme specificities were sourced from major online databases including Kyoto Encyclopedia of Genes and Genomes (KEGG) Glycan, Consortium for Functional Glycomics (CFG), Carbohydrate-Active enZymes (CAZy), GlycoGene DataBase (GGDB) and BRENDA. Based on the known activities, more than 1.1 million theoretical structures and 4.7 million synthetic reactions were generated and stored in our database called UniCorn. Furthermore, we analyzed the differences between the predicted glycan structures in UniCorn and those contained in UniCarbKB (www.unicarbkb.org), a database which stores experimentally described glycan structures reported in the literature, and demonstrate that UniCorn can be used to aid in the assignment of ambiguous structures whilst also serving as a discovery database.

#### Citation
PubMed : https://www.ncbi.nlm.nih.gov/pubmed/27318307

#### Acknowledgements
This work was supported by the Macquarie University Research Excellence Scheme Postgraduate Co-tutelle Scholarship; the Australian National eResearch Collaboration Tools and Resources (NeCTAR) project (RT016); and Soka University.

#### Human N-glycan Enzyme List

| Number | Enzyme | Id |
| ------ | ------ | -- |
| 1 | 	Histo-blood group ABO system transferase | 	ABO
| 2 | 	Beta-1,3-galactosyltransferase 1 | 	B3GALT1
| 3 | 	Beta-1,3-galactosyltransferase 2 | 	B3GALT2
| 4 | 	Beta-1,4-galactosyltransferase 1 | 	B4GALT1
| 5 | 	Beta-1,4-galactosyltransferase 2 | 	B4GALT2
| 6 | 	Beta-1,4-galactosyltransferase 3 | 	B4GALT3
| 7 | 	Beta-1,4-galactosyltransferase 4 | 	B4GALT4
| 8 | 	Beta-1,4-N-acetylgalactosaminyltransferase 3 | 	B4GALNT3
| 9 | 	N-acetyl-beta-glucosaminyl-glycoprotein 4-beta-N-acetylgalactosaminyltransferase 1 | 	B4GALNT4
| 10 | 	Galactoside 2-alpha-L-fucosyltransferase 1 | 	FUT1 | 
| 11 | 	Galactoside 2-alpha-L-fucosyltransferase 2 | 	FUT2 | 
| 12 | 	Galactoside 3(4)-L-fucosyltransferase | 	FUT3 | 
| 13 | 	Alpha-(1,3)-fucosyltransferase 4 | 	FUT4 | 
| 14 | 	Alpha-(1,3)-fucosyltransferase 5 | 	FUT5 | 
| 15 | 	Alpha-(1,3)-fucosyltransferase 6 | 	FUT6 | 
| 16 | 	Alpha-(1,3)-fucosyltransferase 7 | 	FUT7 | 
| 17 | 	Alpha-(1,6)-fucosyltransferase 8 | 	FUT8 | 
| 18 | 	Alpha-(1,3)-fucosyltransferase 9 | 	FUT9 | 
| 19 | 	Alpha-(1,3)-fucosyltransferase 11 | 	FUT11 | 
| 20 | 	Beta-1,3-galactosyl-O-glycosyl-glycoprotein beta-1,6-N-acetylglucosaminyltransferase 3 | GCNT3 | 
| 21 | 	Galactosylgalactosylxylosylprotein 3-beta-glucuronosyltransferase 1 | 	B3GAT1 | 
| 22 | 	Galactosylgalactosylxylosylprotein 3-beta-glucuronosyltransferase 2 | 	B3GAT2 | 
| 23 | 	Alpha-1,3-mannosyl-glycoprotein 2-beta-N-acetylglucosaminyltransferase | 	MGAT1 | 
| 24 | 	Alpha-1,6-mannosyl-glycoprotein 2-beta-N-acetylglucosaminyltransferase | 	MGAT2 | 
| 25 | 	Beta-1,4-mannosyl-glycoprotein 4-beta-N-acetylglucosaminyltransferase | 	MGAT3 | 
| 26 | 	Alpha-1,3-mannosyl-glycoprotein 4-beta-N-acetylglucosaminyltransferase A | 	MGAT4A | 
| 27 | 	Alpha-1,3-mannosyl-glycoprotein 4-beta-N-acetylglucosaminyltransferase B | 	MGAT4B | 
| 28 | 	Alpha-1,3-mannosyl-glycoprotein 4-beta-N-acetylglucosaminyltransferase C | 	MGAT4C | 
| 29 | 	Alpha-1,6-mannosylglycoprotein 6-beta-N-acetylglucosaminyltransferase A | 	MGAT5 | 
| 30 | 	Alpha-1,6-mannosylglycoprotein 6-beta-N-acetylglucosaminyltransferase B | 	MGAT5B | 
| 31 | 	CMP-N-acetylneuraminate-beta-galactosamide-alpha-2,3-sialyltransferase 1 | 	ST3GAL1 | 
| 32 | 	CMP-N-acetylneuraminate-beta-galactosamide-alpha-2,3-sialyltransferase 2 | 	ST3GAL2 | 
| 33 | 	CMP-N-acetylneuraminate-beta-1,4-galactoside alpha-2,3-sialyltransferase | 	ST3GAL3 | 
| 34 | 	CMP-N-acetylneuraminate-beta-galactosamide-alpha-2,3-sialyltransferase 4 | 	ST3GAL4 | 
| 35 | 	Type 2 lactosamine alpha-2,3-sialyltransferase | 	ST3GAL6 | 
| 36 | 	Beta-galactoside alpha-2,6-sialyltransferase 1	| ST6GAL1 | 
| 37 | 	Beta-galactoside alpha-2,6-sialyltransferase 2 | 	ST6GAL2 | 
| 38 | 	Alpha-N-acetylneuraminide alpha-2,8-sialyltransferase | 	ST8SIA1 | 
| 39 | 	Alpha-2,8-sialyltransferase 8B | 	ST8SIA2 | 
| 40 | 	Sia-alpha-2,3-Gal-beta-1,4-GlcNAc-R:alpha 2,8-sialyltransferase | 	ST8SIA3 | 
| 41 | 	CMP-N-acetylneuraminate-poly-alpha-2,8-sialyltransferase | 	ST8SIA4 | 
| 42 | 	Alpha-2,8-sialyltransferase 8E | 	ST8SIA5 | 
| 43 | 	Alpha-2,8-sialyltransferase 8F | 	ST8SIA6 | 
| 44 | 	Galactosylgalactosylxylosylprotein 3-beta-glucuronosyltransferase 1 | 	B3GAT1 | 
| 45 | 	Galactosylgalactosylxylosylprotein 3-beta-glucuronosyltransferase 2 | 	B3GAT2 | 

